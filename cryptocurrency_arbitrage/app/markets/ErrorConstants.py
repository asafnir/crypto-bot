"""

This file defines all the contants required for exchange modules

"""


class ErrorConstants():
    # Error codes for exception handling
    EXCHANGE_ERROR = "EXCHANGE_ERR"
    NETWORK_ERROR = "NETWORK_ERR"
    BALANCE_CHECK_ERROR = "BALANCE_CHECK_ERR"
    NO_BALANCE_ERROR = "NO_BALANCE_ERR"
    TICKER_ERROR = "TICKER_ERR"
    EXECUTION_ERROR = "EXECUTION_ERR"
