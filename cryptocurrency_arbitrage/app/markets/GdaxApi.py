from Exchange import Exchange
from Constants import Constants
from GDAX import *


class GdaxApi(Exchange):
    """
        Gdax class that inherits from Exchange class and functions specific to Gdax api
        Attributes:
            apikey(string)  Api Key
            apisecret(string) Api Secret
            apipass(string) PassPhrase for accessing the api

    """

    def __init__(self, apikey, apisecret, apipass):
        self.API = gdax(apikey, apisecret, apipass)

    def getBalance(self):
        """
        Get Balance of all currencies
        :return: return all coins and fiat currency balances
        """
        gdaxBalance = self.API.get_balances()
        return gdaxBalance

    # TODO :  Currently only limit order is supported, to be extended
    def sell(self, coin, amount, limitprice):
        """
        Function to place sell  order

        :param coin: Coin that you want to sell
        :param amount: Amount of coins that you would like to sell
        :param limitprice: Minimum price that you would accept for one coin
        :return: result of the sell order
        """
        response = self.API.place_limit_order('sell', coin, limitprice, amount)
        return response

    # TODO :  Currently only limit order is supported, to be extended
    def buy(self, coin, amount, limitprice):
        """
        Function to place buy order
        :param coin: Coin that you want to buy
        :param amount: Amount of coins that you would like to buy
        :param limitprice: Maximum price that you would pay for one coin
        :return: result of the buy order
        """
        response = self.API.place_limit_order('buy', coin, limitprice, amount)
        return response

    def getTicker(self, coin):
        """
        Get price of particular coin in the market
        :param coin: Symbol of the coin fiat pair
        :return: Price of the coin in the Gemini market
        """
        coinPrice = float(self.API.get_ticker(coin)['price'])
        return coinPrice

    def getBuyBalance(self):
        """
        Get dollars available for buying any coin in Gdax

        :return: Total Dollars available as deposit for executing buy trades
        """
        allBalance = self.getBalance()

        # This logic really depends on the api response and can vary in time

        for currencyBalance in allBalance:
            if currencyBalance["currency"] == Constants.GdaxUSDollar:
                return currencyBalance["available"]

        # Return 0 if there was no entry for us dollars
        return 0

    def getSellBalance(self, coin):
        """

        Get balance of particular coin available for selling

        :param coin: the coin that you are trying to sell
        :return: amount of coins that are available for sell
        """
        allBalance = self.getBalance()
        for currencyBalance in allBalance:
            if currencyBalance["currency"] == Constants.GdaxCoinSymbols[coin]:
                return currencyBalance["available"]

        # Return 0 if there was no entry for the coin
        return 0
