from django.db import models
from django.template.loader import render_to_string, get_template
from pymongo import MongoClient
from bson.objectid import ObjectId
from django.template import Context
from django.core.mail import EmailMultiAlternatives
from random import randint

client = MongoClient('localhost', 27017)
db = client['beesmart']


class Login(models.Model):
    def sendRequest(name, password):
        collection = db['userlist']
        request = {
            'name': name,
            'pass': password,
            'confirm': True
        }
        ret = collection.find(request).count()
        return ret


class Register(models.Model):
    def sendRequest(name, password, email):
        strGreeting = 'Hello '

        collection = db['cryptoarbitrage']

        hashCode = randint(12000, 99999)
        # request = {
        #     'name' : name,
        #     'pass' : password,
        #     'email' : email,
        #     'confirm' : True,
        #     'hash' : hashCode,
        #     'gdaxapi': "",
        #     'gdaxsecret': "",
        #     'gdaxpass': "",
        #     'geminikey': "",
        #     'geminisecret': "",
        #     'krakenkey': "",
        #     'krakensecret': "",
        #     'botid': -1,
        # }

        {
            "_id": ObjectId("5a215912498ff12236691ca8"),
            "name": "test",
            "pass": "",
            "email": "",
            "confirm": "",
            "hash": "",
            "gdaxsecret": "a7KOEF3QmRQ/TKP8GwfyMIwgyi649om84Tv5y/N/F6S1hKWrkTJUagbjwJHq86+xbcGLT4iKCgpQvdrMkl7MIg==",
            "gdaxpass": "jeboyoaazpl",
            "geminikey": "qApEwFoL2qztTiCkkOAh",
            "geminisecret": "oXAfpFaGaW7k7rv7YezJ9RN9SnV",
            "krakenkey": "A5Ra1NSZ4LG7NjEpq2XRPyJSVZjyc0JqFqt3A6NSugvlMwcbchmnmID8",
            "krakensecret": "uDP0fH9dWopb5sdynJ2GgPDBvBSihgyXUItDZs2jh6jRv/vJEP1f7leLZQ8Q3fxSNRXbVuIxKHtgCwu4NjQdNw==",
            "botid": 98779,
            "poloapikey": "a",
            "polosecret": "a",
            "gdaxkey": "603e48e47b3f2a184c5cca86049ffa53",
            "transfee": 0.2,
            "tolerance": "0.01",
            "amount": 10.0,
            "useKraken": "true",
            "useGemini": "true",
            "useGdax": "true",
            "feeKraken": "0.02",
            "feeGemini": "0.02",
            "feeGdax": "0.02",
            "ubGdax": "false",
            "ueGdax": "false",
            "beGdax": "false",
            "ubGemini": "true",
            "ueGemini": "true",
            "beGemini": "false",
            "ubKraken": "true",
            "ueKraken": "true",
            "beKraken": "true",
            "ueKrarken": "false"
        }

        ret = collection.find({'name': name, 'email': email}).count()
        if (ret > 0): return -1
        if (ret == 0):
            ret = collection.insert_one(request).inserted_id
            #            d = Context({ 'mainCode' : hashCode})
            #            message = get_template('verify.html').render(d)
            #            msg = EmailMultiAlternatives(str(strGreeting) + str(name), message, to=[email])
            #            msg.attach_alternative(message, "text/html")
            #            msg.send()
            return 1


class Confirm(models.Model):
    def sendRequest(name, password, email, hashnumber):
        collection = db['cryptoarbitrage']
        ret = collection.find({"name": name, "pass": password, "email": email, "hash": int('0' + hashnumber)}).count()
        print(name)
        print(password)
        print(email)
        print(hashnumber)
        print(ret)
        if (ret == 1):
            collection.update_one({"name": name, "pass": password, "email": email, "hash": int('0' + hashnumber)},
                                  {'$set': {'confirm': True}})
        return ret


class CheckName(models.Model):
    def sendRequest(name):
        collection = db['cryptoarbitrage']
        ret = collection.find({'name': name}).count()
        return ret


class Bots(models.Model):
    def connectKey(apikey, secret, name, gpass, exchange):
        print("HERE CONNECT KEY")
        print(exchange)
        collection = db['cryptoarbitrage']
        #        mycollection = collection.find_one({'name' : name})
        # if ( exchange == "Poloniex" ):
        #     collection.update_one({'name': name}, {'$set':{'poloapikey': apikey, 'polosecret': secret}}, upsert=False)
        # elif ( exchange == "Bitfinex" ):
        #     collection.update_one({'name': name}, {'$set':{'bitapikey': apikey, 'bitsecret': secret}}, upsert=False)
        # if ( exchange == "gdax" ):
        #     collection.update_one({'name': name}, {'$set':{'gdaxkey': apikey, 'gdaxsecret': secret, 'gdaxpass': gpass}}, upsert=False)
        # elif ( exchange == "gemini" ):
        #     collection.update_one({'name': name}, {'$set':{'geminikey': apikey, 'geminisecret': secret}}, upsert=False)
        # elif ( exchange == "kraken" ):
        #     collection.update_one({'name': name}, {'$set':{'krakenapi': apikey, 'krakensecret': secret}}, upsert=False)
        print(apikey)
        print(secret)
        print(name)

    def getBotParam(name):
        collection = db['cryptoarbitrage']
        #        mycollection = collection.find_one({'name' : name})
        ret = collection.find_one({'name': name})
        return ret

    def setRunBotParam(name, useGemini, useKraken, useGdax, tolerance, opportunity, amount, runinterval, testMode):
        collection = db['cryptoarbitrage']
        collection.update_one({'name': name}, {
            '$set': {'useKraken': useKraken, 'useGemini': useGemini, 'useGdax': useGdax, 'tolerance': tolerance,
                     'opportunity': opportunity, 'amount': amount, 'runinterval': runinterval, 'testMode': testMode}},
                              upsert=False)

    def setExchangeParam(apikey, secret, gpass, fee, name, ub, ue, be, exchange):
        collection = db['cryptoarbitrage']

        if (exchange == 'Gdax'):
            collection.update_one({'name': name}, {
                '$set': {'gdaxkey': apikey, 'gdaxsecret': secret, 'gdaxpass': gpass, 'feeGdax': fee, 'ubGdax': ub,
                         'ueGdax': ue, 'beGdax': be}}, upsert=False)
        elif (exchange == 'Gemini'):
            collection.update_one({'name': name}, {
                '$set': {'geminikey': apikey, 'geminisecret': secret, 'feeGemini': fee, 'ubGemini': ub, 'ueGemini': ue,
                         'beGemini': be}}, upsert=False)
        elif (exchange == 'Kraken'):
            collection.update_one({'name': name}, {
                '$set': {'krakenkey': apikey, 'krakensecret': secret, 'feeKraken': fee, 'ubKraken': ub, 'ueKrarken': ue,
                         'beKraken': be}}, upsert=False)

    def setStatus(name, status, exchange):
        collection = db['cryptoarbitrage']
        #        mycollection = collection.find_one({'name' : name})
        if (exchange == "Poloniex"):
            collection.update_one({'name': name}, {'$set': {'polobotStatus': status}}, upsert=False)
        elif (exchange == "Bitfinex"):
            collection.update_one({'name': name}, {'$set': {'bitbotStatus': status}}, upsert=False)

    def setPID(name, pid, exchange):
        collection = db['cryptoarbitrage']
        #        mycollection = collection.find_one({'name' : name})
        # if ( exchange == "Poloniex" ):
        #     collection.update_one({'name': name}, {'$set':{'polopid': pid}}, upsert=False)
        # elif ( exchange == "Bitfinex" ):
        #     collection.update_one({'name': name}, {'$set':{'bitpid': pid}}, upsert=False)
        collection.update_one({'name': name}, {'$set': {'botid': pid}}, upsert=False)

    def addPastBot(botid, startTime, botTime, repitation, profit):
        strGreeting = 'Hello '

        collection = db['pastbots']

        hashCode = randint(12000, 99999)
        # request = {
        #     'name' : name,
        #     'pass' : password,
        #     'email' : email,
        #     'confirm' : True,
        #     'hash' : hashCode,
        #     'gdaxapi': "",
        #     'gdaxsecret': "",
        #     'gdaxpass': "",
        #     'geminikey': "",
        #     'geminisecret': "",
        #     'krakenkey': "",
        #     'krakensecret': "",
        #     'botid': -1,
        # }

        request = {
            'botid': '1',
            'startTime': startTime,
            'botTime': botTime,
            'repitation': repitation,
            'profit': profit
        }

        #        ret = collection.find({'name' : name, 'email' : email}).count()
        #        if (ret > 0): return -1
        ret = 0
        if (ret == 0):
            ret = collection.insert_one(request).inserted_id
            #            d = Context({ 'mainCode' : hashCode})
            #            message = get_template('verify.html').render(d)
            #            msg = EmailMultiAlternatives(str(strGreeting) + str(name), message, to=[email])
            #            msg.attach_alternative(message, "text/html")
            #            msg.send()
            return 1

    def getPastBot():

        collection = db['pastbots']

        ret = collection.find()

        return ret
